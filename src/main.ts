import { createApp } from "vue";
import App from "./App.vue";
import SocketIO from "socket.io-client";
import VueSocketIO from "vue-socket.io";
import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
  history: createWebHistory(),
  routes: [],
});

const vueSocket = new VueSocketIO({
  debug: true,
  // connection: "http://webback.thettdoc.com:8080/",
  connection: "http://localhost:3000",
  //   connection: SocketIO("ws://webback.thettdoc.com:8080/socket.io"),
});

createApp(App).use(router).use(vueSocket).mount("#app");
