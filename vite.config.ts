import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  server: {
    proxy: {
      "/api": {
        target: "http://webback.thettdoc.com:8080",
        changeOrigin: true,
        configure: (proxy, options) => {
          // proxy will be an instance of 'http-proxy'
        },
      },
      "/socket.io": {
        // target: "ws://webback.thettdoc.com:8080/socket.io",
        target: "ws://webback.thettdoc.com:8080/socket.io",
        changeOrigin: true,
        ws: true,
      },
    },
  },
});
